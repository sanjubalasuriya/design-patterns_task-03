package Strategy_abominado;

public class Context {
	private Strategy strategy1 = new ConcreteStrategyA();
	private Strategy strategy2= new ConcreteStrategyB();
	private Strategy strategy3 = new ConcreteStrategyC();

	public void execute1() {
		strategy1.executeAlgorithm();
	}

	public void setStrategy1(Strategy strategy) {
		strategy1 = strategy1;
	}

	public Strategy getStrategy1() {
		return strategy1;
	}
	
	//Strategy 2
	public void execute2() {
		strategy2.executeAlgorithm();
	}

	public void setStrategy2(Strategy strategy) {
		strategy2 = strategy2;
	}

	public Strategy getStrategy2() {
		return strategy2;
	}

	
	
	//Strategy 3
	public void execute3() {
		strategy3.executeAlgorithm();
	}

	public void setStrategy3(Strategy strategy) {
		strategy3 = strategy3;
	}

	public Strategy getStrategy3() {
		return strategy3;
	}

}
